﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2a;
using Zadaca2a.Klase;

namespace CentralaTests
{
    [TestClass]
    public class TestPosaljiPoruku
    {
        Sekretarica s, s2;
        Korisnik k;
        Korisnik primaoc;
        List<Korisnik> primaoci;
        List<Poruka> poruke;
        Poruka p1, p2, p3, p4;
        Centrala c;

        [TestInitialize()]
        public void Initialize()
        {
            k = new Korisnik("Bakir", "Karovic", "387", DateTime.Now, "1411965320008472", 1500);
            s = new Sekretarica(k);
            primaoc = new Korisnik("Test", "ovic", "033", DateTime.Now, "1411965320008472", 1600);
            s2 = new Sekretarica(primaoc);
            primaoci = new List<Korisnik>();
            primaoci.Add(primaoc);
            poruke = new List<Poruka>();
            p1 = new Poruka("hi", DateTime.Now, k, primaoci);
            p2 = new Poruka("wazzup", DateTime.Now, k, primaoci);
            p3 = new Poruka("hello", DateTime.Now, k, primaoci);
            c = new Centrala("Bugojno");
            c.registrujSekretaricu(s);
            c.registrujSekretaricu(s2);
        }

        [TestMethod]
        public void KorektnoPoslane()
        {
            List<Korisnik> neuspjelaSlanja = c.posaljiPoruku(p1);
            CollectionAssert.AreEqual(new List<Korisnik>(), neuspjelaSlanja);
        }

        [TestMethod]
        public void PoslanePoruke()
        {
            List<Korisnik> neuspjelaSlanja = c.posaljiPoruku(p1);
            //test je li poruka u odlaznom sanducetu posiljaoca
            List<Poruka> listaPoslanihPoruka = new List<Poruka>();
            listaPoslanihPoruka.Add(p1);
            CollectionAssert.AreEqual(listaPoslanihPoruka, s.PoslanePoruke);
        }

        [TestMethod]
        public void PoslanePorukeReferenca()
        {
            c.posaljiPoruku(p1);
            foreach (Poruka temp in s.PoslanePoruke)
            {
                Assert.AreSame(p1, temp);
            }
        }

        [TestMethod]
        public void PristiglePoruke()
        {
            List<Korisnik> neuspjelaSlanja = c.posaljiPoruku(p1);
            //test je li poruka dosla kod primaoca
            List<Poruka> listaPrimljenihPoruka = new List<Poruka>();
            listaPrimljenihPoruka.Add(p1);
            CollectionAssert.AreEqual(listaPrimljenihPoruka, s2.PristiglePoruke);
        }

        [TestMethod]
        public void PristiglePorukeReferenca()
        {
            c.posaljiPoruku(p1);
            foreach (Poruka temp in s2.PristiglePoruke)
            {
                Assert.AreSame(p1, temp);
            }
        }

        [TestMethod]
        public void VracenePoruke()
        {
            Korisnik k3 = new Korisnik("Denisula", "Destovic", "387", DateTime.Now, "6011816573426759", 678);
            primaoci.Add(k3);
            Poruka p4 = new Poruka("inace", DateTime.Now, k, primaoci);
            List<Korisnik> neuspjelaSlanja = c.posaljiPoruku(p4);
            //provjera je li poruka vracena jer nije pristigla
            List<Korisnik> listaExpected = new List<Korisnik>();
            listaExpected.Add(k3);
            CollectionAssert.AreEqual(listaExpected, neuspjelaSlanja);
        }

        [TestMethod]
        public void NeprimljenePoruke()
        {
            Korisnik k3 = new Korisnik("Denisula", "Destovic", "387", DateTime.Now, "6011816573426759", 678);
            Sekretarica s3 = new Sekretarica(k3);
            primaoci.Add(k3);
            Poruka p4 = new Poruka("inace", DateTime.Now, k, primaoci);
            List<Korisnik> neuspjelaSlanja = c.posaljiPoruku(p4);
            //provjera je li poruka dosla jer nebi trebala da jeste
            List<Poruka> poslate = new List<Poruka>();
            poslate.Add(p4);
            CollectionAssert.AreNotEqual(poslate, s3.PristiglePoruke);
            
        }
    }
}
