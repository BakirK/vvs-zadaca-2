﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2a;
using Zadaca2a.Klase;


namespace CentralaTests
{
    [TestClass]
    public class TestRegistrujSekretaricu
    {
        static Sekretarica s;
        static Korisnik k;
        Korisnik primaoc;
        List<Korisnik> primaoci;
        List<Poruka> poruke;
        Poruka p1, p2, p3, p4;
        Centrala c;

        [TestInitialize()]
        public void Initialize()
        {
            k = new Korisnik("Bakir", "Karovic", "387", DateTime.Now, "1411965320008472", 1500);
            s = new Sekretarica(k);
            primaoc = new Korisnik("Test", "ovic", "033", DateTime.Now, "1411965320008472", 1600);
            primaoci = new List<Korisnik>();
            primaoci.Add(primaoc);
            poruke = new List<Poruka>();
            p1 = new Poruka("hi", DateTime.Now, k, primaoci);
            p2 = new Poruka("wazzup", DateTime.Now, k, primaoci);
            p3 = new Poruka("hello", DateTime.Now, k, primaoci);
            p4 = new Poruka("bonjur", DateTime.Now, k, primaoci);
            c = new Centrala("Bugojno");
        }

        [TestMethod]
        public void RegistrujIsteSekretarice()
        {
            Sekretarica s2 = s;
            c.registrujSekretaricu(s);
            Assert.IsFalse(c.registrujSekretaricu(s2));
        }

        [TestMethod]
        public void RegistrujRazliciteSekretarice()
        {
            Sekretarica s2 = new Sekretarica(primaoc);
            Sekretarica s3 = new Sekretarica(new Korisnik("Petar Karadjorjevic", "Bogoljub sedamnaesti", 
                                                            "420", DateTime.Now, "4012888888881881", 33));
            c.registrujSekretaricu(s);
            Assert.IsTrue(c.registrujSekretaricu(s2)); //s i s2 i s3 su razlicite sekretarice
            Assert.IsTrue(c.registrujSekretaricu(s3));
        }
    }
}
