﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2a;
using Zadaca2a.Klase;


namespace SekretaricaTests
{
    [TestClass]
    public class TestPosaljiPoruku
    {
        static Sekretarica s;
        static Korisnik k;
        Korisnik primaoc;
        List<Korisnik> primaoci;
        List<Poruka> poruke;
        Poruka p1, p2, p3, p4;

        [TestInitialize()]
        public void Initialize()
        {
            k = new Korisnik("Bakir", "Karovic", "387", DateTime.Now, "1411965320008472", 1500);
            s = new Sekretarica(k);
            primaoc = new Korisnik("Test", "ovic", "033", DateTime.Now, "1411965320008472", 1500);
            primaoci = new List<Korisnik>();
            primaoci.Add(primaoc);
            poruke = new List<Poruka>();
            p1 = new Poruka("hi", DateTime.Now, k, primaoci);
            p2 = new Poruka("wazzup", DateTime.Now, k, primaoci);
            p3 = new Poruka("hello", DateTime.Now, k, primaoci);
            p4 = new Poruka("bonjur", DateTime.Now, k, primaoci);
        }

        [TestMethod]
        public void DvijePoruke()
        {
            poruke.Add(p1);
            poruke.Add(p2);
            s.spremiPoslanuPoruku(p1);
            s.spremiPoslanuPoruku(p2);
            CollectionAssert.AreEqual(poruke, s.PoslanePoruke);
        }

        [TestMethod]
        public void IzbaciPorukeFIFO()
        {
            s.spremiPoslanuPoruku(p1);
            s.spremiPoslanuPoruku(p2);
            s.spremiPoslanuPoruku(p3);
            s.spremiPoslanuPoruku(p4);
            poruke.Add(p3);
            poruke.Add(p4);
            CollectionAssert.AreEqual(poruke, s.PoslanePoruke);
        }


        [TestMethod]
        public void IzbaciPorukeLIFO()
        {
            //ako su poruke pristigle u isto vrijeme, 
            //ne izbaci onu na vecem indeksu iako je ona starija jer ne gleda ms ili mikro sekunde
            //pa je dodano >= kod compare
            s.Algoritam = "LIFO";
            s.spremiPoslanuPoruku(p1);
            s.spremiPoslanuPoruku(p2);
            s.spremiPoslanuPoruku(p3);
            s.spremiPoslanuPoruku(p4);
            poruke.Add(p1);
            poruke.Add(p4);
            CollectionAssert.AreEqual(poruke, s.PoslanePoruke);
        }

        [TestMethod]
        public void UspjesoSpremioPoslanuPoruku()
        {
            Assert.IsTrue(s.spremiPoslanuPoruku(p1));
        }

    }
}
