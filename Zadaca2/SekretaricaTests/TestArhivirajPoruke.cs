﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2a;
using Zadaca2a.Klase;


namespace SekretaricaTests
{
    [TestClass]
    public class TestArhivirajPoruke
    {
        static Sekretarica s;
        static Korisnik k;
        Korisnik primaoc;
        List<Korisnik> primaoci;
        List<Poruka> poruke;
        Poruka p1, p2, p3, p4;

        [TestInitialize()]
        public void Initialize()
        {
            k = new Korisnik("Bakir", "Karovic", "387", DateTime.Now, "1411965320008472", 1500);
            s = new Sekretarica(k);
            primaoc = new Korisnik("Test", "ovic", "033", DateTime.Now, "1411965320008472", 1500);
            primaoci = new List<Korisnik>();
            primaoci.Add(primaoc);
            poruke = new List<Poruka>();
            p1 = new Poruka("hi", DateTime.Now, k, primaoci);
            p2 = new Poruka("wazzup", DateTime.Now, k, primaoci);
            p3 = new Poruka("hello", DateTime.Now, k, primaoci);
            p4 = new Poruka("bonjur", DateTime.Now, k, primaoci);
        }

        [TestMethod]
        public void ArhivirajPoslanePoruke()
        {
            s.povecajKapacitet(1);

            s.spremiPoslanuPoruku(p1);
            s.spremiPoslanuPoruku(p2);
            s.spremiPoslanuPoruku(p3);

            List<Int32> indeksi = new List<int>();
            indeksi.Add(0); indeksi.Add(1); //indeksi se shiftaju na nize nakon delete

            s.arhivirajPoruke(indeksi, "Poslane Poruke"); //brise p1 i p3 (neintuitivno)

            poruke.Add(p2);
            CollectionAssert.AreEqual(poruke, s.PoslanePoruke);
        }

        [TestMethod]
        public void ArhivirajPristiglePoruke()
        {
            s.povecajKapacitet(2);

            s.primiPoruku(p1);
            s.primiPoruku(p2);
            s.primiPoruku(p3);
            s.primiPoruku(p4);

            List<Int32> indeksi = new List<int>();
            indeksi.Add(0); indeksi.Add(0); //indeksi se shiftaju na nize nakon delete

            s.arhivirajPoruke(indeksi, "Pristigle Poruke"); //brise p1 i p2 (neintuitivno)

            poruke.Add(p3); poruke.Add(p4);
            CollectionAssert.AreEqual(poruke, s.PristiglePoruke);
        }
    }
}
