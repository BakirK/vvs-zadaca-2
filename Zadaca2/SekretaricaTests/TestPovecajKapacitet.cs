﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2a;
using Zadaca2a.Klase;

namespace SekretaricaTests
{
    [TestClass]
    public class TestPovecajKapacitet
    {
        static Korisnik k;
        [TestInitialize()]
        public void Initialize()
        {
            k = new Korisnik("Bakir", "Karovic", "387", DateTime.Now, "1411965320008472", 25000);
        }

        [TestMethod]
        public void CijenaBezPopusta()
        {
            Sekretarica s = new Sekretarica(k);
            String st = s.povecajKapacitet(15);
            StringAssert.Contains(st, "sa 2 na 17 poruka, a to vas je koštalo =23.25 KM.");
        }

        [TestMethod]
        public void CijenaSaMaloPopusta()
        {
            Sekretarica s = new Sekretarica(k);
            String st = s.povecajKapacitet(40);
            StringAssert.EndsWith(st, "sa 2 na 42 poruka, a to vas je koštalo =58.9 KM.");
        }

        [TestMethod]
        public void CijenaPuniPoputst()
        {
            Korisnik k = new Korisnik("Bakir", "Karovic", "387", DateTime.Now, "1411965320008472", 209.25);
            Sekretarica s = new Sekretarica(k);
            String st = s.povecajKapacitet(150);
            StringAssert.StartsWith(st, "Kapacitet uspješno povećan sa 2 na 152 poruka, a to vas je koštalo =209.25 KM.");
        }

        [TestMethod]
        public void KapacitetNula()
        {
            Korisnik k = new Korisnik("Bakir", "Karovic", "387", DateTime.Now, "1411965320008472", 0);
            Sekretarica s = new Sekretarica(k);
            String st = s.povecajKapacitet(0);
            StringAssert.Equals(st, "Kapacitet uspješno povećan sa 2 na 2 poruka, a to vas je koštalo =0 KM.");
        }

        [TestMethod]
        public void KapacitetNegativan()
        {
            //zaradim para ako je kapacitet negativan (exploit)
            Sekretarica s = new Sekretarica(k);
            String st = s.povecajKapacitet(-15000);
            StringAssert.Contains(st, "sa 2 na -14998 poruka, a to vas je koštalo =-23250 KM.");
        }
    }

    
}
