﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2;
using Zadaca2a.Klase;

namespace KarticaTests
{
    [TestClass]
    public class TestValidirajBrojKartice
    {
        static Kartica k = new Kartica();
        [TestMethod]
        public void TacanBrojkartice()
        {
            Assert.IsTrue(k.validirajBrojkartice("1010000071009373"));
            Assert.IsTrue(k.validirajBrojkartice("1411965320008472"));
        }

        public void PogresanBrojkartice()
        {
            Assert.IsFalse(k.validirajBrojkartice("1411965320008476"));
            //Assert.IsFalse(k.validirajBrojkartice("4012888888881888"));
        }

        [TestMethod]
        public void BrojKarticeSaSlovima()
        {
            Assert.IsFalse(k.validirajBrojkartice("a4119653k0008476"));
        }
    }
}
