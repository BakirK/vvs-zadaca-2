﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2;
using Zadaca2a.Klase;

namespace KarticaTests
{
    [TestClass]
    public class TestNaplati
    {
        [TestMethod]
        public void TestNaplatiInt()
        {
            Kartica k = new Kartica("1411965320008472", 100000);
            k.naplati(45700);
            Assert.AreEqual(54300, k.Stanje);
        }

        [TestMethod]
        public void TestNaplatiDecimal()
        {
            Kartica k = new Kartica("1411965320008472", 50000.01);
            k.naplati(49999.99);
            Assert.AreEqual(0.02, Math.Round(k.Stanje), 2);
        }
    }
}
