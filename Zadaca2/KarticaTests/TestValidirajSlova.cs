﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2;
using Zadaca2a.Klase;

namespace KarticaTests
{
    [TestClass]
    public class TestValidirajSlova
    {
        [TestMethod]
        public void SamoSlova()
        {
            Assert.IsTrue(Kartica.validirajSlova("qwertyaz"));
        }

        [TestMethod]
        public void JuznoslavenskaSlova()
        {
            Assert.IsFalse(Kartica.validirajSlova("žćčđš"));
        }

        [TestMethod]
        public void Brojevi()
        {
            Assert.IsFalse(Kartica.validirajSlova("165"));
        }

        [TestMethod]
        public void ZnakoviIBrojevi()
        {
            Assert.IsFalse(Kartica.validirajSlova("J16-L-B38:?"));
        }

        [TestMethod]
        public void JapanskiBrojevi()
        {
            Assert.IsFalse(Kartica.validirajSlova("ぬふあうえおやゆよわ"));
        }
    }
}
