﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2a;
using Zadaca2a.Izuzeci;
using Zadaca2a.Klase;

namespace UnitTestKapacitet
{

    [TestClass]
    public class UnitTestKapacitetICijena
    {
        const Double POPUST_NA_KOLIICNU_20 = 0.05; // Popust na kolicinu iznad 20 poruka.
        const Double POPUST_NA_KOLIICNU_50 = 0.1; // Popust na kolicinu iznad 50 poruka.
        const Double CIJENA_KAPACITETA = 1.55; // Cijena jednog dodatnog mjesta kapaciteta.
        Int32 KAPACITET = 2;


        [TestMethod]
        public void TestPovecajKapacitetZaMali()
        {
            Korisnik k = new Korisnik("Harun", "Hadzic", "061281192", DateTime.Today, "3540386395237955", 1000);
            Sekretarica s = new Sekretarica(k);
            int povecanje = 15;

            String rez = s.povecajKapacitet(povecanje);

            Double iznos = 0;

            iznos = povecanje * CIJENA_KAPACITETA;

            String assert = "Kapacitet uspješno povećan sa " + Convert.ToString(KAPACITET);

            KAPACITET = KAPACITET + povecanje;
            assert += " na " + Convert.ToString(KAPACITET) + " poruka, a to vas je koštalo =" + Convert.ToString(iznos) + " KM.";

            StringAssert.Equals(assert, rez);
        }


        [TestMethod]
        public void TestPovecajKapacitetZaSrednji()
        {
            Korisnik k = new Korisnik("Harun", "Hadzic", "061281192", DateTime.Today, "3540386395237955", 1000);
            Sekretarica s = new Sekretarica(k);
            int povecanje = 45;

            String rez = s.povecajKapacitet(povecanje);

            Double iznos = 0;

            iznos = (povecanje * CIJENA_KAPACITETA) * (1 - POPUST_NA_KOLIICNU_20);

            String assert = "Kapacitet uspješno povećan sa " + Convert.ToString(KAPACITET);

            KAPACITET = KAPACITET + povecanje;
            assert += " na " + Convert.ToString(KAPACITET) + " poruka, a to vas je koštalo =" + Convert.ToString(iznos) + " KM.";

            StringAssert.Equals(assert, rez);
        }

        [TestMethod]
        public void TestPovecajKapacitetZaVeliki()
        {
            Korisnik k = new Korisnik("Harun", "Hadzic", "061281192", DateTime.Today, "3540386395237955", 1000);
            Sekretarica s = new Sekretarica(k);
            int povecanje = 60;

            String rez = s.povecajKapacitet(povecanje);

            Double iznos = 0;

            iznos = (povecanje * CIJENA_KAPACITETA) * (1 - POPUST_NA_KOLIICNU_50);

            String assert = "Kapacitet uspješno povećan sa " + Convert.ToString(KAPACITET);

            KAPACITET = KAPACITET + povecanje;
            assert += " na " + Convert.ToString(KAPACITET) + " poruka, a to vas je koštalo =" + Convert.ToString(iznos) + " KM.";

            StringAssert.Equals(assert, rez);
        }

        [TestMethod]
        public void TestNaplati()
        {
            Kartica kartica = new Kartica("3540386395237955", 1000);

            kartica.naplati(30);

            Assert.AreEqual(970, kartica.Stanje);
        }

        [TestMethod, ExpectedException(typeof(NotEnoughMoneyException))]
        public void TestNaplatiPraznaKartica()
        {
            Kartica kartica = new Kartica("3540386395237955", 0);

            kartica.naplati(30);
        }
    }
}
