﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2a;
using Zadaca2a.Izuzeci;
using Zadaca2a.Klase;

namespace UnitTestIzuzetci
{

    [TestClass]
    public class TestIzuzetciSekretarica
    {
        [TestMethod, ExpectedException(typeof(AlgorithmNotValidException))]
        public void TestPrimiPorukuIzuzetak()
        {
            Korisnik k = new Korisnik("Harun", "Hadzic", "061281192", DateTime.Today, "3540386395237955", 1000);
            Sekretarica s = new Sekretarica(k);
            Poruka p2 = new Poruka();
            Poruka p3 = new Poruka();
            s.PristiglePoruke.Add(p2);
            s.PristiglePoruke.Add(p3);

            s.Algoritam = "Nepostojeci";
            Poruka p = new Poruka();

            s.primiPoruku(p);
        }

        [TestMethod, ExpectedException(typeof(AlgorithmNotValidException))]
        public void TestSpremiPoslanuPorukuIzuzetak()
        {
            Korisnik k = new Korisnik("Harun", "Hadzic", "061281192", DateTime.Today, "3540386395237955", 1000);
            Sekretarica s = new Sekretarica(k);

            Poruka p2 = new Poruka();
            Poruka p3 = new Poruka();
            s.PoslanePoruke.Add(p2);
            s.PoslanePoruke.Add(p3);

            s.Algoritam = "Nepostojeci";
            Poruka p = new Poruka();

            s.spremiPoslanuPoruku(p);
        }



        [TestMethod, ExpectedException(typeof(Exception))]
        public void TestArhivirajPorukeIzuzetak()
        {
            Korisnik k = new Korisnik("Harun", "Hadzic", "061281192", DateTime.Today, "3540386395237955", 1000);
            Sekretarica s = new Sekretarica(k);
            List<Int32> lista = new List<Int32>();
            lista.Add(3);
            s.arhivirajPoruke(lista, "Nempostojeci tip");
        }
    }

    [TestClass]
    public class UnitTestKarticaIzuzetci
    {

        [TestMethod, ExpectedException(typeof(CreditCardNotValidException))]
        public void TestBrojKarticeIzuzetak()
        {
            Kartica k = new Kartica();
            k.BrojKreditneKartice = "99";
        }

        [TestMethod, ExpectedException(typeof(CreditCardNotValidException))]
        public void TestStanjeKarticeIzuzetak()
        {
            Kartica k = new Kartica();
            k.Stanje = -99;
        }

        [TestMethod, ExpectedException(typeof(NotEnoughMoneyException))]
        public void TestNaplatiPraznuKarticuIzuzetak()
        {
            Kartica kartica = new Kartica("3540386395237955", 0);
            kartica.naplati(99);
        }
    }


    [TestClass]
    public class UnitTestPorukeIzuzetci
    {

        [TestMethod, ExpectedException(typeof(MessageTooLongException))]
        public void TestPredukaPorukaIzuzetak()
        {
            Poruka p = new Poruka();
            p.TekstPoruke = "lalallalallalallalalllalallalallalalallalllalallalallalalallalaalallalallalalallalalallalal";
        }

        [TestMethod, ExpectedException(typeof(Exception))]
        public void TestPrioritetPorukeIzuzetak()
        {
            Poruka p = new Poruka();
            p.Prioritet = 99;
        }
    }
}
