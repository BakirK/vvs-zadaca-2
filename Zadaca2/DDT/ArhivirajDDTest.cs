﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadaca2a;
using Zadaca2a.Klase;

namespace DDT
{
    [TestClass]
    public class ArhivirajDDTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }


        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "korisnici.xml", "Korisnik", DataAccessMethod.Sequential), DeploymentItem("korisnici.xml"), TestMethod]

        public void ArhivirajTestXML()
        {
            Korisnik k = new Korisnik(Convert.ToString(TestContext.DataRow["Ime"]),
                                    Convert.ToString(TestContext.DataRow["Prezime"]),
                                    Convert.ToString(TestContext.DataRow["BrojTelefona"]),
                                    Convert.ToDateTime(TestContext.DataRow["DatumRodenja"]),
                                    Convert.ToString(Convert.ToInt64(TestContext.DataRow["BrojKreditneKartice"])),
                                    Convert.ToInt64(TestContext.DataRow["Stanje"]));
            Sekretarica s = new Sekretarica(k);
            Poruka p1 = new Poruka(Convert.ToString(TestContext.DataRow["TekstPoruke"]),
                                    Convert.ToDateTime(TestContext.DataRow["DatumSlanja"]), k, null);
            s.spremiPoslanuPoruku(p1);

            List<Int32> indeksi = new List<int>();
            indeksi.Add(0);
            s.arhivirajPoruke(indeksi, "Poslane Poruke");
            List<Poruka> poruke = new List<Poruka>();
            poruke.Add(p1);
            CollectionAssert.AreEqual(poruke, s.ArhiviranePoruke);
        }


        

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", 
                    "|DataDirectory|\\korisnici.csv", "korisnici#csv", 
                    DataAccessMethod.Sequential), DeploymentItem("korisnici.csv"), TestMethod]
        public void ArhivirajTestCSV()
        {
            Korisnik k = new Korisnik(Convert.ToString(TestContext.DataRow["Ime"]), 
                                    Convert.ToString(TestContext.DataRow["Prezime"]), 
                                    Convert.ToString(TestContext.DataRow["BrojTelefona"]),
                                    Convert.ToDateTime(TestContext.DataRow["DatumRodenja"]),
                                    Convert.ToString(Convert.ToInt64(TestContext.DataRow["BrojKreditneKartice"])),
                                    Convert.ToInt64(TestContext.DataRow["Stanje"]));
            Sekretarica s = new Sekretarica(k);
            Poruka p1 = new Poruka(Convert.ToString(TestContext.DataRow["TekstPoruke"]),
                                    Convert.ToDateTime(TestContext.DataRow["DatumSlanja"]), k, null);
            s.spremiPoslanuPoruku(p1);

            List<Int32> indeksi = new List<int>();
            indeksi.Add(0);
            s.arhivirajPoruke(indeksi, "Poslane Poruke");
            List<Poruka> poruke = new List<Poruka>();
            poruke.Add(p1);
            CollectionAssert.AreEqual(poruke, s.ArhiviranePoruke);
        }
    }
}
